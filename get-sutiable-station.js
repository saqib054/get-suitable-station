/*	
	Link stations are located at points (x, y) and have reach (r) ([x, y, r]):
	Print out function output from points (x, y): (0,0), (100, 100), (15,10) and (18, 18).
*/

/**
  * calculate distance
  * @param array coordinatesArray1.
  * @param array coordinatesArray2.
  * @returns {number}.
  */
function calculateDistance(coordinatesArray1, coordinatesArray2) {
	return Math.hypot(coordinatesArray1[0] - coordinatesArray2[0], coordinatesArray1[1] - coordinatesArray2[1]);
}

/**
  * calculate power
  * @param number reach.
  * @param number distance.
  * @returns {number}.
  */
function calculatePower(reach, distance) {
	return distance > reach ? 0 : Math.pow((reach - distance), 2);
}

/**
  * modify link stations
  * @param array linkStations.
  * @returns {Object[]}.
  */

function modifyLinkStations(linkStations) {
	return linkStations.map(station => {
		return { coordinates: station.slice(0, 2), reach: station[2], power: 0 };
	});
}

/**
  * get suitable station
  * @param point coordinates x and y.
  * @param number distance.
  * @returns Array of objects
  */
function getSuitableStation(point, linkStations) {
	const stations = modifyLinkStations(linkStations).map((station) => {
		station.power = calculatePower(station.reach, calculateDistance(station.coordinates, point));
		return station;
	});
	return stations.sort((station1, station2) => station1.power - station2.power).pop();
}

const linkStations = [[0, 0, 10], [20, 20, 5], [10, 0, 12]];
const points = [[0, 0], [100, 100], [15, 10], [18, 18]];

// display output
points.map(point => {
	const suitableStation = getSuitableStation(point, linkStations);
	if (suitableStation.power !== 0) {
		console.log('Best link station for point ' + point[0] + ',' + point[1] + ' is ' + suitableStation.coordinates + ' with power ' + suitableStation.power);
	} else {
		console.log('No link station within reach for point ' + point[0] + ',' + point[1]);
	}
});
